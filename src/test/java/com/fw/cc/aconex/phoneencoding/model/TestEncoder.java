package com.fw.cc.aconex.phoneencoding.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by sesa279194 on 5/04/2016.
 */
public class TestEncoder {
    private Encoder encoder;

    @Before
    public void before() {
        Char2Digit char2Digit = Mockito.mock(Char2Digit.class);
        Mockito.when(char2Digit.get('J')).thenReturn(5);
        Mockito.when(char2Digit.get('A')).thenReturn(2);
        Mockito.when(char2Digit.get('V')).thenReturn(8);
        Mockito.when(char2Digit.get('A')).thenReturn(2);

        Mockito.when(char2Digit.get('C')).thenReturn(2);
        Mockito.when(char2Digit.get('S')).thenReturn(7);
        Mockito.when(char2Digit.get('H')).thenReturn(4);
        Mockito.when(char2Digit.get('A')).thenReturn(2);
        Mockito.when(char2Digit.get('R')).thenReturn(7);
        Mockito.when(char2Digit.get('P')).thenReturn(7);
        Dictionary dictionary = new Dictionary(char2Digit);
        dictionary.addWord("java");
        dictionary.addWord("csharp");
        this.encoder = new Encoder(dictionary);
    }

    @Test
    public void testEncode() {
        String numbers = "0-5282.1.2742773";
        String[] wordEncodings = this.encoder.encode(numbers);
        Assert.assertEquals("\"" + numbers + "\"' has only 1 word encoding.", 1, wordEncodings.length);
        Assert.assertEquals("\"" + numbers + "\"' has only 1 word encoding which is \"0-JAVA-1-CSHARP-3\".", "0-JAVA-1-CSHARP-3", wordEncodings[0]);
    }
}
