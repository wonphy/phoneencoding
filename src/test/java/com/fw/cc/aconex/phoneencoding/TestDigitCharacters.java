package com.fw.cc.aconex.phoneencoding;

import com.fw.cc.aconex.phoneencoding.model.Char2Digit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by sesa279194 on 5/04/2016.
 */
public class TestDigitCharacters {
    private Char2Digit char2Digit;

    @Before
    public void before() {
        this.char2Digit = new DigitCharacters();
    }

    @Test
    public void testEncode() {
        Assert.assertEquals("B's number is 2.", 2, this.char2Digit.get('B'));
        Assert.assertEquals("V's number is 8.", 8, this.char2Digit.get('V'));
        Assert.assertEquals("-'s number is 0.", 0, this.char2Digit.get('-'));
    }
}
