package com.fw.cc.aconex.phoneencoding.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Set;

/**
 * Created by sesa279194 on 5/04/2016.
 */
public class TestDictionary {

    private Dictionary dictionary;

    @Before
    public void before() {
        Char2Digit char2Digit = Mockito.mock(Char2Digit.class);
        Mockito.when(char2Digit.get('J')).thenReturn(5);
        Mockito.when(char2Digit.get('A')).thenReturn(2);
        Mockito.when(char2Digit.get('V')).thenReturn(8);
        Mockito.when(char2Digit.get('A')).thenReturn(2);

        Mockito.when(char2Digit.get('C')).thenReturn(2);
        Mockito.when(char2Digit.get('S')).thenReturn(7);
        Mockito.when(char2Digit.get('H')).thenReturn(4);
        Mockito.when(char2Digit.get('A')).thenReturn(2);
        Mockito.when(char2Digit.get('R')).thenReturn(7);
        Mockito.when(char2Digit.get('P')).thenReturn(7);
        this.dictionary = new Dictionary(char2Digit);
    }

    @Test
    public void testAddWord() {
        String java = "Java";
        this.dictionary.addWord(java);
        Assert.assertEquals("Java added to the dictionary save as \"JAVA\".", "JAVA", this.dictionary.addWord(java));
        Assert.assertEquals("JAVA's mapping numbers is \"5282\"", "5282", this.dictionary.getWordNumbers(java.toUpperCase()));
    }

    @Test
    public void testReplace() {
        String numbers = "0-5282.1";
        this.dictionary.addWord("java");
        this.dictionary.addWord("csharp");
        Set<String> replaced1=this.dictionary.replace(numbers, "-");
        Assert.assertEquals("\"" + numbers + "\" should be replaced only 1 time.", 1, replaced1.size());
        Assert.assertEquals("\"" + numbers + "\" should be replaced to \"0--JAVA-.1\".", "0--JAVA-.1", replaced1.iterator().next());

        numbers = "0-5282.1.2742773";
        Set<String> replaced2=this.dictionary.replace(numbers, "-");
        String[] replaced2Array = replaced2.toArray(new String[replaced2.size()]);
        Assert.assertEquals("\"" + numbers + "\" should be replaced 2 times.", 2, replaced2.size());
        Assert.assertEquals("\"" + numbers + "\" first replacement should be \"0--JAVA-.1.2742773\".", "0--JAVA-.1.2742773", replaced2Array[0]);
        Assert.assertEquals("\"" + numbers + "\" second replacement should be \"0-5282.1.-CSHARP-3\".", "0-5282.1.-CSHARP-3", replaced2Array[1]);
    }

    @After
    public void after() {
        this.dictionary.clear();
    }
}
