package com.fw.cc.aconex.phoneencoding;

import com.fw.cc.aconex.phoneencoding.model.Char2Digit;

/**
 * An implementation of Char2Digit.
 *
 * Created by Fei Wang on 5/04/2016.
 */
public class DigitCharacters implements Char2Digit {
    public static final String[] DIGIT_CHARACTERS = new String[]{
            "ABC",  // 2
            "DEF",  // 3
            "GHI",  // 4
            "JKL",  // 5
            "MNO",  // 6
            "PQRS", // 7
            "TUV",  // 8
            "WXYZ"  // 9
    };

    public DigitCharacters() {

    }

    @Override
    public int get(char c) {
        int offset = 2;
        for (int i = 0; i < DIGIT_CHARACTERS.length; i++) {
            String cs = DIGIT_CHARACTERS[i];
            if (-1 != cs.indexOf(c)) {
                return i + offset;
            }
        }
        return 0;
    }
}
