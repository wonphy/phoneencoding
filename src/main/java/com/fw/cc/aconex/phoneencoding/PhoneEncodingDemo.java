package com.fw.cc.aconex.phoneencoding;

import com.fw.cc.aconex.phoneencoding.model.Char2Digit;
import com.fw.cc.aconex.phoneencoding.model.Dictionary;
import com.fw.cc.aconex.phoneencoding.model.Encoder;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Aconex 1-800-CODING-CHALLENGE.
 *
 * Created by Fei Wang on 2/04/2016.
 */
public final class PhoneEncodingDemo {
    public static final String OPTION_DICTIONARY = "-d";

    private final Encoder encoder;
    private final String phoneFile;

    /**
     * Creates dictionary, encoder; Saves the phone file.
     *
     * @param dictFile
     * @param phoneFile
     * @throws Exception
     */
    private PhoneEncodingDemo(String dictFile, String phoneFile) throws Exception {
        Char2Digit char2Digit = new DigitCharacters();
        Dictionary dictionary = new Dictionary(char2Digit);
        Files.lines(Paths.get(dictFile)).forEach(w -> dictionary.addWord(w));
        this.encoder = new Encoder(dictionary);
        this.phoneFile = phoneFile;
    }

    /**
     * Encodes each phone and prints out word encodings;
     *
     * @throws Exception
     */
    public void start() throws Exception {
        Files.lines(Paths.get(phoneFile)).map(p -> p.trim()).distinct().forEach(p -> {
            System.out.println(p);
            for (String ep : encoder.encode(p)) {
                System.out.println("\t" + ep);
            }
            System.out.println("");
        });
    }

    public static void main(String[] args) {
        String dict_file = "";
        String phone_file = "";
        boolean dictNext = false;
        for (String arg : args) {
            if (OPTION_DICTIONARY.equals(arg)) {
                dictNext = true;
            } else if (dictNext) {
                dict_file = arg;
                dictNext = false;
            } else {
                phone_file = arg;
            }
        }

        Scanner scanner = new Scanner(System.in);
        while ("".equals(dict_file)) {
            System.out.println("Please provide the dictionary file: ");
            dict_file = scanner.next();
        }
        while ("".equals(phone_file)) {
            System.out.println("Please provide the phone number file: ");
            phone_file = scanner.next();
        }

        try {
            PhoneEncodingDemo phoneEncoding = new PhoneEncodingDemo(dict_file, phone_file);
            phoneEncoding.start();
            System.out.println("Press any key to quit.");
            System.in.read();
            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
