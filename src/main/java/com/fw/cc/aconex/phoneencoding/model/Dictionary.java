package com.fw.cc.aconex.phoneencoding.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Fei Wang on 5/04/2016.
 */
public class Dictionary {
    private final Char2Digit char2Digit;
    private final Map<String, String> word2NumbersMap;

    public Dictionary(Char2Digit digitCharMapper) {
        this.char2Digit = digitCharMapper;
        this.word2NumbersMap = new HashMap<>();
    }

    /**
     * Gets the number string of a word.
     *
     * @param word
     * @return
     */
    private String word2Numbers(String word) {
        StringBuilder numbersSB = new StringBuilder(word.length());
        for (char c : word.toCharArray()) {
            int n = this.char2Digit.get(c);
            numbersSB.append(n);
        }
        return numbersSB.toString();
    }

    /**
     * Adds a word to the dictionary.
     *
     * @param word
     */
    public String addWord(String word) {
        if (null != word) {
            String word2 = word.replaceAll("[^a-zA-Z]", "").toUpperCase();
            if (0 < word2.length()) {
                String numbers = this.word2Numbers(word2);
                this.word2NumbersMap.put(word2, numbers);
                return word2;
            }
        }
        return null;
    }

    public String getWordNumbers(String word) {
        return this.word2NumbersMap.get(word);
    }

    public void clear() {
        this.word2NumbersMap.clear();
    }

    /**
     * Replaces a string number by suitable words.
     *
     * @param numbers
     * @param boundary
     * @return
     */
    public Set<String> replace(String numbers, String boundary) {
        return word2NumbersMap.keySet().stream().parallel()
                .filter(w -> -1 != numbers.indexOf(word2NumbersMap.get(w)))
                .map(w -> numbers.replace(word2NumbersMap.get(w), boundary + w + boundary))
                .collect(Collectors.toSet());
    }
}
