package com.fw.cc.aconex.phoneencoding.model;

/**
 * Created by Fei Wang on 5/04/2016.
 */
public interface Char2Digit {
    /**
     * Returns number which maps to the char.
     *
     * @param c
     * @return
     */
    int get(char c);
}
