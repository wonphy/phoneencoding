package com.fw.cc.aconex.phoneencoding.model;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Gets the dictionary, encodes a number string.
 *
 * Created by sesa279194 on 5/04/2016.
 */
public class Encoder {
    private static final String WORD_BOUNDARY = "-";
    private Dictionary dictionary;

    public Encoder(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * A Breadth-first recursion continue encoding method.
     *
     * @param encodingNumbers
     * @param allWordEncodingSet
     */
    private void encoding(String encodingNumbers, Set<String> allWordEncodingSet) {
        Set<String> wordEncodingSet = this.dictionary.replace(encodingNumbers, WORD_BOUNDARY);
        if (wordEncodingSet.isEmpty()) {
            allWordEncodingSet.add(encodingNumbers);
        } else {
            for (String we : wordEncodingSet) {
                this.encoding(we, allWordEncodingSet);
            }
        }
    }

    /**
     * Converts a word encoding to empty if it contains two ore more consecutive digits;
     * Clean word boundaries.
     *
     * @param wordEncoding
     * @return
     */
    private String filter(String wordEncoding) {
        Pattern pattern = Pattern.compile("[\\d]{2,}");
        Matcher matcher = pattern.matcher(wordEncoding);
        if (!matcher.find()) {
            String wordEncoding2 = wordEncoding
                    .replaceAll("^\\-", "")
                    .replaceAll("\\-$", "")
                    .replaceAll("(\\-+)", "-");
            return wordEncoding2;
        }
        return "";
    }

    /**
     * Entry method of encoding a number string.
     *
     * @param numbers
     * @return
     */
    public String[] encode(String numbers) {
        String numbers2 = numbers.replaceAll("[^\\d]", "");
        Set<String> allWordEncodingSet = new HashSet<>();
        encoding(numbers2, allWordEncodingSet);
        Set<String> validEncodedPhones = allWordEncodingSet.stream()
                .map(ep -> filter(ep))
                .filter(ep -> !ep.isEmpty())
                .collect(Collectors.toSet());
        return validEncodedPhones.toArray(new String[validEncodedPhones.size()]);
    }
}
